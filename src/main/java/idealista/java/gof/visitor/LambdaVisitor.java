package idealista.java.gof.visitor;

public class LambdaVisitor {

	static class Square {
		final double side;

		Square(double side) {
			this.side = side;
		}
	}

	static class Circle {
		final double radius;

		Circle(double radius) {
			this.radius = radius;
		}
	}

	static class Rectangle {
		final double width;
		final double height;

		Rectangle(double width, double height ) {
			this.width = width;
			this.height = height;
		}
	}
}
